declare module "v-mask" {
  import { DirectiveFunction, PluginObject } from "vue";

  const VMask: PluginObject<VMask>;
  const VueMaskDirective: DirectiveFunction;

  export { VueMaskDirective };
  export default VMask;
}
