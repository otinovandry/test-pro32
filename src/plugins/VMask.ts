import Vue from "vue";
import VueMask from "v-mask";

Vue.use(VueMask, {
  placeholders: {
    "#": null,
    D: /\d/,
    C: /\d|[a-fA-F]/,
    Я: /[\wа-яА-Я]/,
  },
});
